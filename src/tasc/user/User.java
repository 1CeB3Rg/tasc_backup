package tasc.user;
import tasc.calendar_event.Notification;

import java.io.Serializable;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.*;

public abstract class User implements Serializable{
    private HashMap<Object,Object> properties;
    private ArrayList<Notification> notifications;
    //Constructor
    public User(){
        this.properties= new HashMap<Object,Object>();
        this.notifications= new ArrayList<Notification>();
    }

    //TODO
    public void addNotifications(Notification arg){}
    public void removeNotifications(Notification arg){}

    //Accessor Methods
    public void setProperties(HashMap<Object,Object> arg){
        this.properties= arg;
    }
    public HashMap<Object, Object> getProperties(){
        return this.properties;
    }
}
