package tasc.user;

import tasc.calendar_event.Appointment;
import tasc.calendar_event.Reminder;
import tasc.course.Course;

import java.util.ArrayList;

public class Regular_user extends User{
    private ArrayList<Appointment> listAppointment;
    private ArrayList<Reminder> listReminder;
    private ArrayList<Course> courses;
    //Constructor
    public Regular_user(){
        super();
        this.listAppointment=new ArrayList<Appointment>();
        this.courses=new ArrayList<Course>();
        this.listReminder=new ArrayList<Reminder>();
    }
    //Accessor Method
    public ArrayList<Appointment> getListAppointment(){return this.listAppointment;}
    public ArrayList<Reminder> getListReminder(){return this.listReminder;}
    public ArrayList<Course> getCourses(){return this.courses;}
    public void setListAppointment(ArrayList<Appointment> arg){this.listAppointment=arg;}
    public void setListReminder(ArrayList<Reminder> arg){this.listReminder=arg;}
    public void setCourses(ArrayList<Course> arg){this.courses=arg;}
}
