package tasc.course;

import tasc.calendar_event.Schedule;
import tasc.user.User;

import java.util.ArrayList;

public class Course {
    private String coursename;
    private Schedule courseSchedule;
    private String courseCode;
    private ArrayList<User> participants;

    public Course(){
        this.participants=new ArrayList<User>();
    }
    //Accessor Method
    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    public void setCourseSchedule(Schedule courseSchedule) {
        this.courseSchedule = courseSchedule;
    }

    public void setParticipants(ArrayList<User> participants) {
        this.participants = participants;
    }

    public String getCoursename() {
        return coursename;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public ArrayList<User> getParticipants() {
        return participants;
    }

    public Schedule getCourseSchedule() {
        return courseSchedule;
    }
}
