package tasc.calendar_event;

import tasc.user.Student;
import tasc.user.Ta;
import tasc.user.User;

import java.util.ArrayList;

public class Appointment extends Calendar_event{
    enum Statuses{Accepted,Rejected,Pending}
    private Student creator;
    private Ta target;
    private String purpose;
    private ArrayList<User> viewer;
    private Statuses status;
    //Constructor
    public Appointment(){
        this.viewer=new ArrayList<User>();
    }
    //Accessor Method

    public Student getCreator() {
        return creator;
    }

    public void setCreator(Student creator) {
        this.creator = creator;
    }

    public Ta getTarget() {
        return target;
    }

    public void setTarget(Ta target) {
        this.target = target;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public ArrayList<User> getViewer() {
        return viewer;
    }

    public void setViewer(ArrayList<User> viewer) {
        this.viewer = viewer;
    }

    public Statuses getStatus() {
        return status;
    }

    public void setStatus(Statuses status) {
        this.status = status;
    }
}
