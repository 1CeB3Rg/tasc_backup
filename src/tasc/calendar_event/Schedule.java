package tasc.calendar_event;

public class Schedule extends Calendar_event{
    private String scheduleName;
    private String request;

    //Accessor Method
    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
