package tasc.calendar_event;

public class Reminder extends Calendar_event{
    private String reminderTitle;

    public String getReminderTitle() {
        return reminderTitle;
    }

    public void setReminderTitle(String reminderTitle) {
        this.reminderTitle = reminderTitle;
    }
}
