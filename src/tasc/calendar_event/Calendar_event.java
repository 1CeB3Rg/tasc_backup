package tasc.calendar_event;

import java.sql.Timestamp;
import java.util.Date;

public abstract class Calendar_event {
    private Date date;
    private Timestamp time;
    private int duration;
    //Constructor
    public Calendar_event(){}
    //Accessor Method

    public Date getDate() {
        return date;
    }

    public int getDuration() {
        return duration;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
