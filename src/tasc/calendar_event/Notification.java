package tasc.calendar_event;

import tasc.user.User;

public class Notification {
    private User recipient;
    private String text;
    private String title;
    //Constructor
    //Accessor

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
