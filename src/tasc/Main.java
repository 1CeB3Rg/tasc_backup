package tasc;
import java.util.Locale;
import java.util.Scanner;
import tasc.commandline.*;
public class Main {
    static Scanner input= new Scanner(System.in);
    static boolean flag= true;
    public static void main(String[] args) {
        System.out.println("Choose Which User do you want to Login");
        while(flag){
        switch(input.nextLine().toLowerCase()){
            case "admin": AdminCli.main(new String[0]); flag=false; break;
            case "ta": TaCli.main(new String[0]); flag=false; break;
            case "student": Studentcli.main(new String[0]);flag=false;break;
            default:
                System.out.println("Invalid input, enter ta admin or student");
        }
        }
    }
}
