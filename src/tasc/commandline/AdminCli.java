package tasc.commandline;

import tasc.user.Regular_user;
import tasc.user.Student;
import tasc.user.Ta;
import tasc.user.User;
import tasc.commandline.ObjectIO;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Locale;
import java.util.Scanner;

public class AdminCli {

    private static boolean loop=true;
    private static final Scanner input = new Scanner(System.in);
    private static final String command_list=
            """
            add_user: to create a user
            edit_user: edit a user
            delete_user: delete a user
            assign_course: assign course to user
            exit: quit the application
            """;
    public static void main(String[] args) {
        System.out.println("Logged in as Admin");
        loop=true;
        while(loop){
            query();
        }
    }
    /*
    Method to receive user input based on first command
     */
    public static void query(){
        switch(input.nextLine().toLowerCase()){
            case "add_user":add_user();break;
            case "edit_user":edit_user();break;
            case "delete_user":delete_user();break;
            case "assign_course":assignUsertoCourse();break;
            case "help":
                System.out.println(command_list);break;
            case "exit":
                loop=false;
                System.out.println("Thank you for using the application");
                break;
            default:
                System.out.println("input 'help' for list of commands");
        }
    }
    /*
    Method to create a user and save to repo
    */

    public static void add_user(){
        System.out.println("Select Role:");
        String role;
        switch(input.nextLine().toLowerCase()){
            case "ta":role="ta";break;
            case "student":role="student";break;
            default:
                System.out.println("Not valid, enter ta or student");return;
        }
        System.out.println("Enter a name:");
        String name=input.nextLine();
        //Create object and write into a file
        switch(role){
            case "ta":
                Ta ta= new Ta();
                ta.getProperties().put("Name",name);
                ObjectIO.write_object(ta,"ta",name);
                break;
            case "student":
                Student student= new Student();
                student.getProperties().put("Name",name);

                ObjectIO.write_object(student,"student",name);
                break;
        }

    }
    /*
    A method to edit a user's data/properties
    TODO: retrieve object, then write in user's file
     */
    public static void edit_user(){}
    /*
    A method to delete the corresponding user's file
    TODO: retrieve object, delete the file
     */
    public static void delete_user(){}
    /*
    A method to add a user to a course
    TODO: retrieve the object, write in course's file and write in user's file
     */
    public static void assignUsertoCourse(){}
    //public void getAppointmentPermission(Appointment);
}
