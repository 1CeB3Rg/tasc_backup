package tasc.commandline;

import tasc.user.Student;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectIO {
    public static final String student_directory= "src/tasc/repo/student/";
    public static final String course_directory= "src/tasc/repo/course/";
    public static final String ta_directory= "src/tasc/repo/ta/";
    /*
    A method to write an object to a file
     */
    public static void write_object(Object object,String role,String name){
        String filepath="";
        System.out.println(role);
        switch(role){
            case "student": filepath= student_directory;break;
            case "ta": filepath= ta_directory;break;
            case "course": filepath= course_directory;break;
        }
        System.out.println(filepath);
        //Find for {name}.txt
        filepath=filepath+name+".txt";
        //Create output stream and return the object
        try{
            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(object);
            objectOut.close();
            System.out.println("Successfully saved");
        }

        catch(Exception e){
            e.printStackTrace();}
    }
    /*
    A method to read an object from a file which takes argument on role for which directory
    to choose and throws Exception if the file is not found
    */
    public static Object obtain_object(String role,String name) throws Exception{
        String filepath="";
        switch(role){
            case "student": filepath= student_directory;break;
            case "ta": filepath= ta_directory;break;
            case "course": filepath= course_directory;break;
        }
        //Find for {name}.txt
        filepath=filepath+name+".txt";
        //Create input stream and return the object
        try{
            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            Object output = objectIn.readObject();
            objectIn.close();

            return output;
        }
        catch(Exception e){
            //e.printStackTrace();
            throw new Exception("File Not found");
        }
    }
}
