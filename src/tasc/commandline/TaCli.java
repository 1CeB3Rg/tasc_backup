package tasc.commandline;

import tasc.course.Course;
import tasc.user.Student;
import tasc.user.Ta;

import java.util.ArrayList;
import java.util.Scanner;

public class TaCli {
    private static boolean loop=true;
    private static final Scanner input = new Scanner(System.in);
    private static final String command_list= """
            show: show all courses enrolled
            help: show list of command
            exit: quit the application
            """;
    public static Ta current;
    public static void main(String[] args) {
        System.out.println("Logged in as TA");
        loop=true;
        //Pseudo Login
        //Ask for current user name, check if it is in database, if not then crash TODO: For now let it crash
        System.out.println("Enter Your name (Case Sensitive)");
        String name= input.nextLine();
        //Retrieve the Ta's object
        try{
            current= (Ta) ObjectIO.obtain_object("ta",name);
            System.out.println("Welcome "+current.getProperties().get("Name"));
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("User name not in Database"+" Exiting......");
            return;
        }
        while(loop){
            query();
        }


    }
    /*
    Method to receive user input based on first command
     */
    public static void query(){
        switch(input.nextLine().toLowerCase()){
            case "show": show_courses();break;
            case "help":
                System.out.println(command_list);break;
            case "exit": loop=false;
                System.out.println("Thank you for using the application");break;
            default:
                System.out.println("input 'help' for list of commands");
        }
    }
    /*
    Method to show all courses enrolled
     */
    public static void show_courses(){
        ArrayList<Course> courseList= current.getCourses();
        if(!courseList.isEmpty()){
            for(Course i:courseList){
                System.out.print(i+" ");
            }}
        else{
            System.out.println("User "+current.getProperties().get("Name")+" is not enrolled to any course");
        }
    }
}
